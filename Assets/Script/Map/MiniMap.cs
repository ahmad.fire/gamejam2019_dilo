﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    public GameObject aaa;
  
    public GameObject start;
    public GameObject finish;
    public GameObject mapParent;
    public GameObject objectPrefab;
    public Sprite park;
    public float offsetX;
    public float offsetY;
    private int countX;
    public int[] node;
    public GameObject[,] tiles;
    Camera _camera;
    Vector2 previousNode;
    ArrayList nodes = new ArrayList();
    int[,] arr = {
        { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0},
        { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    };

    // Start is called before the first frame update
    void Start()
    {
       
        tiles = new GameObject[node.Length, 12];
        start = GameObject.Find("start");
        finish = GameObject.Find("finish");
        //offsetX = (finish.transform.position.x - start.transform.position.x) / (node.Length+1);
        offsetX = 1;

        //createNode();
    }

    // Update is called once per frame
    void Update()
    {
       // dottedNode();
           
    }

    void createNode()
    {
        _camera = Camera.main;
        float startY;
        int z = 0;
        for (int x = 0; x < node.Length; x++)
        {
            for (int y = 0; y < node[x]; y++)
            {
                offsetY = (_camera.orthographicSize * 1.5f) / (node[x]);
                if (node[x] % 2 == 0)
                {
                    startY = (offsetY / 2) + (((node[x] - 1) / 2) * offsetY);
                }
                else if (node[x] == 1)
                {
                    startY = 0;
                }
                else
                {
                    startY = ((node[x] - 1) / 2) * offsetY;
                }
                Vector2 previousPos = new Vector3(start.transform.position.x + ((x + 1) * offsetX), startY - ((y) * offsetY));
                GameObject place = Instantiate(objectPrefab, previousPos, objectPrefab.transform.rotation);
                place.transform.parent = mapParent.transform;
                place.name = "0," + z;
                
                tiles[0, z] = place;
                z++;
            }
            start.name = "0,0";
            finish.name = "0" + z+2;
            
        }
    }

    void dottedNode()
    {
        
        //for (int x = 0; x < arr.GetLength(0); x++)
        //{
        //    for (int y = 0; y < arr.GetLength(1); y++)
        //    {
        //        if (arr[x, y] == 1)
        //        {
        //            if (arr[y, x] == 1)
        //            {

        //            }
        //            //Debug.Log(x + "," + y);
        //            DottedLine.DottedLine.Instance.DrawDottedLine(tiles[0, y].transform.position, tiles[0, y].transform.position);
        //        }
        //    }
        //}
    }
    //void dottedNode()
    //{
    //    for (int x = 0; x <= node.Length; x++)
    //    {
    //        if (x == 0)
    //        {
    //            for (int y = 0; y < node[x]; y++)
    //            {
    //                if (x > 1)
    //                {
    //                    if (tiles[x, y] != null)
    //                    {
    //                        previousNode = tiles[x, y].transform.position;
    //                    }
    //                    //previousNode = start.transform.position;
    //                    //Debug.Log(tiles[1, 0].transform.position);
    //                }
    //                else
    //                {
    //                    previousNode = start.transform.position;
    //                }
    //                GameObject currentNode = GameObject.Find(x + "," + y);
    //                DottedLine.DottedLine.Instance.DrawDottedLine(previousNode, currentNode.transform.position);
    //            }
    //        }
    //        else
    //        {
    //            countX = node[x - 1];
    //            for (int y = 0; y < countX; y++)
    //            {
    //                if (tiles[x, y] != null)
    //                {
    //                    previousNode = GameObject.Find(countX + "," + y).transform.position;
    //                }
    //                //previousNode = start.transform.position;
    //                //Debug.Log(tiles[1, 0].transform.position);

    //                GameObject currentNode = GameObject.Find(x + "," + y);
    //                DottedLine.DottedLine.Instance.DrawDottedLine(previousNode, currentNode.transform.position);
    //            }
    //        }
    //    }
    //}
}
