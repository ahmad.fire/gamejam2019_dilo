﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destination : MonoBehaviour
{
    public List<Destination> nextDest;

    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < nextDest.Count; i++)
        {
            if (gameObject == null || nextDest[i].gameObject == null)
            {
                continue;
            }
            DottedLine.DottedLine.Instance.DrawDottedLine(this.gameObject.transform.position, nextDest[i].transform.position);
            
        }
    }
}
